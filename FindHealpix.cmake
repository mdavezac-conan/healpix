add_library(healpix::cxx INTERFACE IMPORTED GLOBAL)

find_path(HEALPIX_INCLUDE_DIR healpix_base.h
          PATH_SUFFIXES healpix_cxx DOC "Healpix include directory")

find_library(HEALPIX_CXX_LIBRARY healpix_cxx)
if(HEALPIX_INCLUDE_DIR)
  set_target_properties(healpix::cxx
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${HEALPIX_INCLUDE_DIR})
endif()

if(HEALPIX_CXX_LIBRARY)
  target_link_libraries(healpix::cxx INTERFACE ${HEALPIX_CXX_LIBRARY})
endif()
set(HEALPIX_LIBRARIES healpix::cxx)


include("FindPackageHandleStandardArgs")
find_package_handle_standard_args(Healpix
  FOUND_VAR HEALPIX_FOUND
  REQUIRED_VARS HEALPIX_LIBRARIES HEALPIX_INCLUDE_DIR HEALPIX_CXX_LIBRARY
)
