from conans import AutoToolsBuildEnvironment, ConanFile, tools


class HealpixConan(ConanFile):
    name = "healpix"
    version = "3.50.0"
    license = "MIT"
    author = "Jet Propulsion Lab, NASA"
    homepage = "https://healpix.jpl.nasa.gov"
    description = "Data Analysis, Simulations and Visualization on the Sphere"
    topics = ("Physics", "AstroPhysics", "Imaging", "Data Analysis")
    settings = "os", "compiler", "build_type", "arch"
    url = "https://gitlab.com/mdavezac-conan/healpix"
    tar_url = "https://sourceforge.net/projects/healpix/files/Healpix_3.50/healpix_cxx-3.50.0.tar.gz/download"
    generators = "cmake"
    options = {"shared": [True, False]}
    default_options = {"shared": False, "cfitsio:shared": False}
    requires = "cfitsio/3.450@AstroFizz/stable"
    exports_sources = ["FindHealpix.cmake"]

    def imports(self):
        self.copy("*.dll", "", "bin")
        self.copy("*.dylib*", "", "lib")
        self.copy("*.so*", "", "lib")

    def source(self):
        from os import rename

        tools.download(self.tar_url, "healpix.tar.gz")
        tools.untargz("healpix.tar.gz")
        rename("healpix_cxx-3.50.0", "healpix")

    @property
    def autotools(self):
        if not hasattr(self, "_autotools"):
            autotools = AutoToolsBuildEnvironment(self)
            args = ["--disable-openmp"]
            if self.options.shared:
                args += ["--disable-static", "--enable-shared"]
            else:
                args += ["--disable-shared", "--enable-static", "--with-pic"]
            autotools.configure(configure_dir="healpix", args=args)
            self._autotools = autotools
        return self._autotools

    def build(self):
        self.autotools.make()

    def package(self):
        self.autotools.make(target="install")
        self.copy("FindHealpix.cmake", ".", ".")

    def package_info(self):
        self.cpp_info.libs = ["healpix_cxx"]
