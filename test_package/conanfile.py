from conans import CMake, ConanFile


class HealpixTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def imports(self):
        self.copy("*.dll", "", "bin")
        self.copy("*.dylib*", "", "lib")
        self.copy("*.so*", "", "lib")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def test(self):
        from os.path import join

        self.run(join("bin", "test_healpix"))
