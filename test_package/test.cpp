#include <healpix_cxx/healpix_map.h>

int main(int nargs, char *argv[]) {
  int const n = 3;
  Healpix_Map<float> hmap(n, RING);
  return hmap.Map().size() == (12 << (n << 1)) ? 0: 1;
}
